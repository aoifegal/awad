class CreateCountries < ActiveRecord::Migration[5.1]
  def change
    create_table "countries", force: true do |t|
      t.string "symbol"
      t.integer "rank"
      t.string "name"
      t.integer :population

      t.timestamps
    end
  end
end
