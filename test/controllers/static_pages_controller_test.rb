require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  test "should get root" do
    get '/'
    assert_response :success
  end
  
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "School Comparison App"
  end
  
  test "should get maps" do
    get maps_path
    assert_response :success
    assert_select "title", "Maps | School Comparison App"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | School Comparison App"
  end
   test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | School Comparison App"
  end
  
  require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "",
                                         email: "user@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'users/new'
  end
end
end 